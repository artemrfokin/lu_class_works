package ru.levelup.lesson2.reflection;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.Random;

public class RandomIntProcessor {

    public static Phone process(Phone phone) throws IllegalAccessException {
        Class phoneClass = phone.getClass();

        Field[] fields = phoneClass.getDeclaredFields();
        setupFields(fields, phone);

        return phone;
    }

    public static <T> T process(Class<T> classOfT) throws IllegalAccessException, InvocationTargetException, InstantiationException {
        Constructor<?> constr = Arrays.stream(classOfT.getDeclaredConstructors())
                .filter(constructor -> constructor.getParameterCount() == 0)
                .findFirst()
                .get();

        constr.setAccessible(true);
        Object instance = constr.newInstance();

        Field[] fields = classOfT.getDeclaredFields();
        setupFields(fields, instance);
       
        return (T) instance;
    }

    private static void setupFields(Field[] fields, Object object) throws IllegalAccessException {
        for (Field field : fields) {
            RandomInt annotation = field.getAnnotation(RandomInt.class);
            if (annotation != null) {
                Random random = new Random();
                int randomInt = random.nextInt(annotation.max() - annotation.min() + 1) + annotation.min();
                field.setAccessible(true);
                field.set(object, randomInt);
            }
        }

    }
    
}
