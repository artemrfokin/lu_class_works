package org.levelup.servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/auth")
public class AuthorisationServlet extends HttpServlet {
    /*
    * аннотации - 3ие сервлеты
    * 2ые веб файл. Дескриптор.
    * cntr + O ввыбор метода перелбпдедегие
    * */

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.getWriter()
            .append("<html><body><h1>Hello world " +
                    "<form method=\"post\" action=\"auth\"><button type=\"submit\">Submit</button</from>" +
                    "</h1</body></html>");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.getWriter().append("Thank you");
    }
}
