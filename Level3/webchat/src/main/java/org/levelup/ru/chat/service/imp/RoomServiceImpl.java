package org.levelup.ru.chat.service.imp;

import org.levelup.ru.chat.domain.DTO.Room;
import org.levelup.ru.chat.domain.Entities.RoomEntity;
import org.levelup.ru.chat.exception.ChatException;
import org.levelup.ru.chat.repositary.RoomRepositary;
import org.levelup.ru.chat.service.RoomService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class RoomServiceImpl extends AbstractService implements RoomService {
    private final RoomRepositary roomRepositary;

    @Autowired
    public RoomServiceImpl(ModelMapper modelMapper, RoomRepositary roomRepositary) {
        super(modelMapper);
        this.roomRepositary = roomRepositary;
    }

    @Override
    public Collection<Room> findAll() {
        Iterable<RoomEntity> values = roomRepositary.findAll();
        return findAllEntities(values, Room.class);
    }

    @Override
    public Room findById(Integer roomId) {
        return roomRepositary.findById(roomId)
                .map(entity -> modelMapper.map(entity, Room.class))
                .orElseThrow(ChatException::new);
    }
}
