package org.levelup.ru.chat.domain.DTO;

import javax.validation.constraints.NotBlank;

public class AuthorizationRequest {

    @NotBlank
    private String login;
    private String password;

    public AuthorizationRequest() {
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
