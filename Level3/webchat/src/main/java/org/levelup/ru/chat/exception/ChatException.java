package org.levelup.ru.chat.exception;

public class ChatException extends RuntimeException{

    public ChatException(){
        super();
    }

    public ChatException(String message){
        super(message);
    }
}
