package org.levelup.ru.chat.service.imp;

import org.levelup.ru.chat.domain.DTO.User;
import org.levelup.ru.chat.domain.Entities.UserEntity;
import org.levelup.ru.chat.repositary.UserRepositary;
import org.levelup.ru.chat.service.AuthenticationService;
import org.levelup.ru.chat.service.UserService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class UserServiceImpl extends AbstractService implements UserService {
    private final UserRepositary userRepositary;
    private final AuthenticationService authenticationService;

    @Autowired
    public UserServiceImpl(AuthenticationService authenticationService, UserRepositary userRepositary, ModelMapper modelMapper) {
        super(modelMapper);
        this.authenticationService = authenticationService;
        this.userRepositary = userRepositary;
    }


    @Override
    public Collection<User> getAll() {
        Iterable<UserEntity> iterable = userRepositary.findAll(); // DTO не привязан к базе, безопасно.
        return findAllEntities(iterable, User.class);
//        return StreamSupport.stream(iterable.spliterator(), false)
//                .map(entity -> modelMapper.map(entity, User.class))
//                .collect(Collectors.toList());
    }


    @Override
    public boolean auth(String login, String password) {
        return authenticationService.authenticate(login, password);
    }
}
