package org.levelup.ru.chat.service;

import org.levelup.ru.chat.domain.DTO.User;

import java.util.Collection;

public interface UserService {
    Collection<User> getAll();
    boolean auth(String login, String password);
}
