package org.levelup.ru.chat.controller;

import org.levelup.ru.chat.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class UserController {
//    @Qualifier("UserServiceImpl")
//private final UserService userServiceImpl; написать конкретный класс с маленькой буквы
    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/users")
    public String displayAllUsers(Model model){
        model.addAttribute("users", userService.getAll());
        return "users";
    }

    /*
    * Bean defenition - cоздат лбюбой Бин
    * BeanDefenitionReader - выбирается один из них.
    * BeanFactory - встроенная фабрика
    * FactoryBean - самописная фабрика.
    * FactoryBeanPostProcessor - предварительная настройка или после испольнения.
    * BeanPostProcessor - теперь после создания бина будет работать.
    *
    * ДЗ
    * Авторизация и отображение еще одной таблички.
    *
    * */
}
