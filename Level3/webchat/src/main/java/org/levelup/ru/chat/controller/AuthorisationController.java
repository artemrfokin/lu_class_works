package org.levelup.ru.chat.controller;

import org.levelup.ru.chat.domain.DTO.AuthorizationRequest;
import org.levelup.ru.chat.domain.DTO.UserSession;
import org.levelup.ru.chat.service.AuthenticationService;
import org.levelup.ru.chat.service.AuthorizationSessionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

@Controller
public class AuthorisationController {
    private final AuthenticationService authService;
    private final AuthorizationSessionService authorizationSessionService;

    @Autowired
    public AuthorisationController(AuthenticationService authService, AuthorizationSessionService authorizationSessionService) {
        this.authService = authService;
        this.authorizationSessionService = authorizationSessionService;
    }

    @GetMapping("/login")
    public String loginPage(Model model){
        model.addAttribute("request", new AuthorizationRequest());
        return "login";
    }

    @PostMapping("/login")
    public String login(@ModelAttribute("request") AuthorizationRequest request,
                        BindingResult bindingResult,
                        HttpServletResponse response){
        boolean isAuthenticated = authService.authenticate(request.getLogin(), request.getPassword());
        if (!isAuthenticated){
            bindingResult.rejectValue("login", "");
            return "login";
        }

        UserSession session = authorizationSessionService.createOrUpdateSession(request.getLogin());
        response.addCookie(new Cookie("WC_SESSION", session.getSid()));
        return "redirect:/users";
    }

}
