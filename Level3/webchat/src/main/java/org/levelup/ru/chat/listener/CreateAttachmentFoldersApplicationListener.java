package org.levelup.ru.chat.listener;

import lombok.SneakyThrows;
import org.levelup.ru.chat.domain.DTO.User;
import org.levelup.ru.chat.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class CreateAttachmentFoldersApplicationListener implements ApplicationListener<ContextRefreshedEvent> {
    @Value("${web.chat.attachment}")
    private String attachmentsPath;
    @Value("${web.chat.attachment.avatars}")
    private String avatarsPath;
    private final UserService userService;

    @Autowired
    public CreateAttachmentFoldersApplicationListener(UserService userService) {
        this.userService = userService;
    }


    @SneakyThrows
    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        boolean attachementFoldersExist = Files.exists(Paths.get(attachmentsPath));
        boolean avatarsPathsFoldersExist = Files.exists(Paths.get(avatarsPath));

        if(!attachementFoldersExist){
            createFolder(attachmentsPath);
        }

        if(!avatarsPathsFoldersExist){
            createFolder(avatarsPath);
        }


        for (User user : userService.getAll()) {
            if(!Files.exists(Paths.get(attachmentsPath + user.getLogin()))){
                Files.createDirectories(Paths.get(attachmentsPath + user.getLogin()));
            }
        }
    }

    @SneakyThrows
    private void createFolder(String path){
        Files.createDirectories(Paths.get(path));
    }
}
