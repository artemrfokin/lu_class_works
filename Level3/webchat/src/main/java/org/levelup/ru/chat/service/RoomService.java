package org.levelup.ru.chat.service;

import org.levelup.ru.chat.domain.DTO.Room;

import java.util.Collection;

public interface RoomService {
    Collection<Room> findAll();
    Room findById(Integer roomId);
}
