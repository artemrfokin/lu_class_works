package org.levelup.ru.chat.service.imp;

import org.levelup.ru.chat.domain.Entities.UserEntity;
import org.levelup.ru.chat.repositary.UserRepositary;
import org.levelup.ru.chat.service.AuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AuthenticationServiceImp implements AuthenticationService {
    private final UserRepositary userRepositary;

    @Autowired
    public AuthenticationServiceImp(UserRepositary userRepositary) {
        this.userRepositary = userRepositary;
    }

    @Override
    public boolean authenticate(String login, String password) {
        UserEntity user = userRepositary.findByLogin(login);
        return user != null && user.getPassword().equals(password);
    }
}
