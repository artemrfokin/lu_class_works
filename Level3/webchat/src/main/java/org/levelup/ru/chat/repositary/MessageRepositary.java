package org.levelup.ru.chat.repositary;

import org.levelup.ru.chat.domain.Entities.MessageEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MessageRepositary extends CrudRepository<MessageEntity, Integer> {
    Iterable<MessageEntity> findByRoomId(Integer roomId);
}
