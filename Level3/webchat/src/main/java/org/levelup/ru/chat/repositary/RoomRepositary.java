package org.levelup.ru.chat.repositary;

import org.levelup.ru.chat.domain.Entities.RoomEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoomRepositary extends CrudRepository<RoomEntity, Integer> {
}
