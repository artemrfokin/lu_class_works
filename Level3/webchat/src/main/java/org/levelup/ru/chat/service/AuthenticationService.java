package org.levelup.ru.chat.service;

public interface AuthenticationService {
    boolean authenticate(String login, String password);
}
