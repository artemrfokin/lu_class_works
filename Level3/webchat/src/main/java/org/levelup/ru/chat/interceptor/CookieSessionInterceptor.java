package org.levelup.ru.chat.interceptor;

import org.levelup.ru.chat.service.AuthenticationService;
import org.levelup.ru.chat.service.AuthorizationSessionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.Optional;

public class CookieSessionInterceptor implements HandlerInterceptor {
    @Autowired
    private AuthorizationSessionService authSessionService;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        Cookie[] cookies = request.getCookies();
        if (cookies == null){
            response.sendRedirect("/login");
            return false;
        }

        Optional<Cookie> possCookie = Arrays.stream(cookies)
                .filter(cookie -> cookie.getName().equals("WC_SESSION"))
                .findFirst();

        System.out.println("possCookie.get() " + possCookie.get().getName());

        if (!possCookie.isPresent()){
            response.sendRedirect("/login");
            return false;
        }

        if (authSessionService.isExpired(possCookie.get().getValue())){
            response.sendRedirect("/login");
            return false;
        }

        return true;
    }
}
