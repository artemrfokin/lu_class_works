package org.levelup.ru.chat.service.imp;

import org.levelup.ru.chat.domain.DTO.Message;
import org.levelup.ru.chat.domain.DTO.MessageData;
import org.levelup.ru.chat.domain.DTO.UserAvatar;
import org.levelup.ru.chat.domain.Entities.MessageEntity;
import org.levelup.ru.chat.domain.Entities.RoomEntity;
import org.levelup.ru.chat.domain.Entities.UserEntity;
import org.levelup.ru.chat.repositary.MessageRepositary;
import org.levelup.ru.chat.repositary.RoomRepositary;
import org.levelup.ru.chat.repositary.UserRepositary;
import org.levelup.ru.chat.service.AuthorizationSessionService;
import org.levelup.ru.chat.service.MessageService;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.stream.Collectors;

@Service
public class MessageServiceImpl extends AbstractService implements MessageService {
    private final AuthorizationSessionService authSessionService;
    private final UserRepositary userRepositary;
    private final RoomRepositary roomRepositary;
    private final MessageRepositary messageRepositary;

    public MessageServiceImpl(ModelMapper modelMapper, AuthorizationSessionService authSessionService,
                              UserRepositary userRepositary, RoomRepositary roomRepositary,
                              MessageRepositary messageRepositary) {
        super(modelMapper);
        this.authSessionService = authSessionService;
        this.userRepositary = userRepositary;
        this.roomRepositary = roomRepositary;
        this.messageRepositary = messageRepositary;
    }

    @Override
    public Collection<Message> findAllMessagesInRoom(Integer roomId) {
        Iterable<MessageEntity> values = messageRepositary.findByRoomId(roomId);
        Collection<Message> allEntities = findAllEntities(values, Message.class);
        return allEntities.stream()
                .peek(entity -> entity.setUser(new UserAvatar("/avatars/default.png", "Natasha")))
                .collect(Collectors.toList());
    }

    @Override
    @Transactional
    public Message saveMessage(String sid, MessageData messageData) {
//        System.out.println("I am in saveMessage");
        String login = authSessionService.findLoginBySessionId(sid);
//        System.out.println("login " + login);
        UserEntity user = userRepositary.findByLogin(login);
//        System.out.println("user.getLogin() " + user.getLogin());
        RoomEntity room = roomRepositary.findById(messageData.getRoomId()).get();
//        System.out.println("room.getName()" + room.getName());

        MessageEntity result = messageRepositary.save(new MessageEntity(messageData.getText(), LocalDateTime.now(), user, room));
        return new Message(result.getId(), result.getText(), new UserAvatar("default.png", "Natasha"));
    }
}
