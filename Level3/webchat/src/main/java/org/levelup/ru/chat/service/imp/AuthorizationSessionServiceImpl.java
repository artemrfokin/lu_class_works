package org.levelup.ru.chat.service.imp;

import org.levelup.ru.chat.domain.DTO.UserSession;
import org.levelup.ru.chat.domain.Entities.AuthSessionEntity;
import org.levelup.ru.chat.domain.Entities.UserEntity;
import org.levelup.ru.chat.exception.ChatException;
import org.levelup.ru.chat.repositary.AuthorizationSessionRepositary;
import org.levelup.ru.chat.repositary.UserRepositary;
import org.levelup.ru.chat.service.AuthorizationSessionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Optional;

@Service
public class AuthorizationSessionServiceImpl implements AuthorizationSessionService {
    private final UserRepositary userRepositary;
    private final AuthorizationSessionRepositary authSessionRepositary;

    @Autowired
    public AuthorizationSessionServiceImpl(UserRepositary userRepositary, AuthorizationSessionRepositary authSessionRepositary) {
        this.userRepositary = userRepositary;
        this.authSessionRepositary = authSessionRepositary;
    }

    @Override
    public UserSession createOrUpdateSession(String login) {
        UserEntity user = userRepositary.findByLogin(login);
        if (user == null){
            throw new RuntimeException();
        }

        Optional<AuthSessionEntity> possSession = authSessionRepositary.findByLogin(login);
        LocalDateTime expireDate = LocalDateTime.now().plusDays(1);

        if (possSession.isPresent()){
            AuthSessionEntity sessionEntity = possSession.get();
            sessionEntity.setExpiredDate(expireDate);
            authSessionRepositary.save(sessionEntity);
            return new UserSession(sessionEntity.getSid(), expireDate, sessionEntity.getUser().getLogin());
        }

        AuthSessionEntity sessionEntity = new AuthSessionEntity();
        sessionEntity.setExpiredDate(expireDate);
        sessionEntity.setUser(user);
        authSessionRepositary.save(sessionEntity);
        return new UserSession(sessionEntity.getSid(), expireDate, sessionEntity.getUser().getLogin());
    }

    @Override
    public boolean isExpired(String sid) {
        return authSessionRepositary.findById(sid)
                .map(session -> session.getExpiredDate().isBefore(LocalDateTime.now()))
                .orElse(true);
    }

    @Override
    public String findLoginBySessionId(String sid) {
        return authSessionRepositary.findById(sid)
                .map(entity -> entity.getUser().getLogin())
                .orElseThrow(ChatException::new);
    }

    @Override
    public void removeSession(String sid) {
        authSessionRepositary.deleteById(sid);
    }
}
