package org.levelup.ru.chat.controller;

import org.levelup.ru.chat.domain.DTO.Message;
import org.levelup.ru.chat.domain.DTO.MessageData;
import org.levelup.ru.chat.service.MessageService;
import org.levelup.ru.chat.service.RoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/rooms")
public class RoomController {
    private final RoomService roomService;
    private final MessageService messageService;

    @Autowired
    public RoomController(RoomService roomService, MessageService messageService) {
        this.roomService = roomService;
        this.messageService = messageService;
    }

    @GetMapping
    public String displayRooms(Model model){
        model.addAttribute("rooms", roomService.findAll());
        return "rooms";
    }

    @GetMapping("/{roomId}")
    public String displayRoom(@PathVariable("roomId") final Integer roomId, Model model){
        model.addAttribute("room", roomService.findById(roomId));
        model.addAttribute("messages", messageService.findAllMessagesInRoom(roomId));
        return "room-by-id";
    }

    @ResponseBody // теперь можно возвращать не только HTML. Но и просто данные
    @PostMapping("/{roomId}/message")
    public Message saveMessage(@PathVariable final Integer roomId,
                               @RequestBody MessageData messageData,
                               @CookieValue("WC_SESSION") final String sid){
//        System.out.println("roomId " + roomId);
//        System.out.println("roomID " + messageData.getRoomId());
//        System.out.println("text" + messageData.getText());
        return messageService.saveMessage(sid, messageData);
    }

}
