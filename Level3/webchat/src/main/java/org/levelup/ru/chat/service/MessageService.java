package org.levelup.ru.chat.service;

import org.levelup.ru.chat.domain.DTO.Message;
import org.levelup.ru.chat.domain.DTO.MessageData;
import org.levelup.ru.chat.domain.Entities.MessageEntity;

import java.util.Collection;

public interface MessageService {

    Collection<Message> findAllMessagesInRoom(Integer roomId);
    Message saveMessage(String sid, MessageData messageData);
}
