package org.levelup.ru.chat.service;

import org.levelup.ru.chat.domain.DTO.UserSession;

public interface AuthorizationSessionService {

    UserSession createOrUpdateSession(String login);
    boolean isExpired(String sid);

    String findLoginBySessionId(String sid);
    void removeSession(String sid);
}
