package org.levelup.ru.chat.repositary;

import org.levelup.ru.chat.domain.Entities.UserEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepositary extends CrudRepository<UserEntity, Integer> { // 1 entity, 2 id in entitry
    UserEntity findByLogin(String login);
}
