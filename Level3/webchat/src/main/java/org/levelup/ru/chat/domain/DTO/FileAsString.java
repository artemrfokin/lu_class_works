package org.levelup.ru.chat.domain.DTO;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class FileAsString {
    private String fileName;
    private String file;
}
