create table if not exists messages(
    id serial primary key,
    text text not null,
    send_date timestamp not null,
    author_id integer not null,
    room_id integer not null,
    constraint messages_author_id_fkey foreign key (author_id) references users(id),
    constraint messages_room_id_fkey foreign key (room_id) references rooms(id)
);

insert into messages (text, send_date, author_id, room_id)
values
    ('Hello World! 1', now(), 1, 1),
    ('Hello World! 1', now(), 2, 1),
    ('Hello World! 1', now(), 1, 1),
    ('Hello World! 1', now(), 2, 1),
    ('Hello World! 2', now(), 1, 2),
    ('Hello World! 2', now(), 1, 2),
    ('Hello World! 3', now(), 1, 3);