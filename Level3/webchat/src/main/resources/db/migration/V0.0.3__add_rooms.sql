create table if not exists rooms(
    id serial primary key,
    name varchar(200) not null unique,
    active boolean not null default true
);

insert into rooms(name)
values
    ('Java'),
    ('Flood'),
    ('C++'),
    ('Dota 2'),
    ('JavaScript');