package ru.levelup.jdbc;

import java.sql.*;

public class JDBCExample {
    static {
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e); // для обнаружения сбоя на стадии загрузки
        }
    }

/*joker sponsors - work future*/
    // URL: jdbc:<vendor_name>://<host>:<port>/<db_name>[options] // или без db_name. delimeter options ;,? optiones time zone ssl
    public static void main(String[] args) throws SQLException {
        Connection connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/Chat",
                "postgres",
                "root");// connection - физическое соединение с базой. Установленное соединение

        /*
        * statement - raw SQL
        * prepared state - внчале объект
        * callable statemnt - хранимые процедуры, их будет вызывать.
        * */
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery("select * from users");

        long maxId = 0;
        long minId = 0;
        while (resultSet.next()){ // одновременно говорит есть или нет + переводит на след элмениет
            long id = resultSet.getLong("id");
            String login = resultSet.getString("login");
            String password = resultSet.getString("password");
            System.out.printf("%d %s %s\n", id, login, password);
            maxId = Math.max(id, maxId);
            minId = Math.min(id, minId);
        }
        /*
        * execute - чтобы получить сразу несколько результатов
        * executeQuery
        * ResultSet - таблица данных в виде итератора
        * */
        PreparedStatement preparedStatement = connection.prepareStatement("insert into users(login, password) values (?, ?)");// только тут данные можно передать
        preparedStatement.setString(1, "test_login" + maxId);
        preparedStatement.setString(2, "test_password");
        int count = preparedStatement.executeUpdate();
        System.out.println("Affected raws " + count);

        PreparedStatement delStament = connection.prepareStatement("delete from users where id = ?");
        delStament.setLong(1, maxId);
        int count2 = delStament.executeUpdate();
        System.out.println("Deleted: " + count2);
    }
}
