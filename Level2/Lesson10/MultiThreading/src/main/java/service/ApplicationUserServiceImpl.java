package service;

import org.hibernate.PropertyValueException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import ru.levelup.jdbc.hbm.domain.ApplicationUser;

import java.util.List;

public class ApplicationUserServiceImpl implements ApplicationUserService {
    private SessionFactory factory;

    public ApplicationUserServiceImpl(SessionFactory factory) {
        this.factory = factory;
    }


    @Override
    public void addUser(String login, String password) {
        if (login != null & password != null) {
            Session session = factory.openSession();
            Transaction transaction = session.beginTransaction();

            ApplicationUser user = new ApplicationUser();
            user.setUserLogin(login);
            user.setPassword(password);
            session.save(user);

            transaction.commit();
            session.close();
        } else throw new PropertyValueException("Fields are filled with wrong parameters",
                "ApplicationUser", "userLogin");
    }


    @Override
    public void updateUserInfo(Integer id, String login, String password) {
        if (id >= 1 & login != null & password != null) {
            Session session = factory.openSession();
            Transaction transaction = session.beginTransaction();

            ApplicationUser user = session.get(ApplicationUser.class, id);
            user.setUserLogin(login);
            user.setPassword(password);
            session.persist(user);

            transaction.commit();
            session.close();
        } else throw new PropertyValueException("Fields are filled with wrong parameters",
                "ApplicationUser", "userLogin");
    }


    @Override
    public void showAllUsers() {
        Session session = factory.openSession();
        Transaction transaction = session.beginTransaction();

        Query query = session.createQuery("from ApplicationUser");
        List<ApplicationUser> result = query.list();
        for (ApplicationUser object : result) {
            System.out.printf("Login: %s, password: %s \n", object.getUserLogin(), object.getPassword());
        }

        transaction.commit();
        session.close();
    }

    @Override
    public void deleteUser(int id) {
        if (id >= 1){
            Session session = factory.openSession();
            Transaction transaction = session.beginTransaction();

            Query query = session.createQuery("delete ApplicationUser where id = :id");
            query.setParameter("id", id);
            query.executeUpdate();

            transaction.commit();
            session.close();
        } else throw new PropertyValueException("Field is filled with wrong parameter",
                "ApplicationUser", "id");
    }

    @Override
    public void authorisation(String login, String password){
        if (login != null & password != null){
            Session session = factory.openSession();
            Transaction transaction = session.beginTransaction();

            Query query = session.createQuery("from ApplicationUser where userLogin= :login and password = :password");
            query.setParameter("login", login);
            query.setParameter("password", password);
            List<ApplicationUser> result = query.list();
            if (result.size() != 0 & result.size() == 1){
                ApplicationUser user = result.get(0);
                System.out.printf("Hello %s! You are authorised.\n", user.getUserLogin());
            } else {
                System.out.printf("You entered wrong login or password.\n");
            }

            transaction.commit();
            session.close();
        } else throw new PropertyValueException("Fields are filled with wrong parameters",
                "ApplicationUser", "userLogin");
    }
}
