package ru.levelup.lesson10;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class ScheduledEx {
    public static void main(String[] args) {
        ScheduledExecutorService ses = Executors.newScheduledThreadPool(10);
        ses.scheduleAtFixedRate(() -> {
            System.out.println("Invoke");
        }, 5, 3, TimeUnit.SECONDS);
    }
}
