package ru.levelup.lesson10.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "message")
public class Message {

    @Id
    private int id;
    private String text;

    @ManyToOne // обозначили дочерность
    @JoinColumn(name = "author_id") // foreign key + плюс создавай колонку author_id
    private User user;


}
