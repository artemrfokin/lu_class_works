package ru.levelup.lesson10.domain;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "user_info")
public class UserDetails {

    @Id
    @OneToOne
    private User user; // создается еще это поле

    private int age;
}

/*
* однонаправленная и двунаправленная связи
* нужно указывать главную
*
* */
