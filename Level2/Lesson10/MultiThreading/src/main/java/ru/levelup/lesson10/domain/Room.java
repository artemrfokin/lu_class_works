package ru.levelup.lesson10.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.util.Collection;

@Entity
@Table(name = "room")
@Setter
@Getter
public class Room {

    @Id
    private int id;
    private String name;

    @ManyToMany(mappedBy = "rooms")
    private Collection<User> usersInRoom;
}
