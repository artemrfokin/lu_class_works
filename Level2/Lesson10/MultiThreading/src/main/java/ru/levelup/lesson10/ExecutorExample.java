package ru.levelup.lesson10;

import java.util.concurrent.*;

public class ExecutorExample {
    public static void main(String[] args) {
//        ExecutorsService service = Executors.newFixedThreadPool(10,
//                r -> {
//
//                });
//    }

        ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(100);
        scheduledExecutorService.schedule(() -> {
        }, 100, TimeUnit.MINUTES); // через 100 минут один раз
        scheduledExecutorService.scheduleWithFixedDelay(() -> {
        }, 0, 100, TimeUnit.SECONDS); // каждый поток через 100 секунд заупстится
        scheduledExecutorService.scheduleAtFixedRate(() -> {
        }, 0, 100, TimeUnit.SECONDS); // делей между задачами 100 млсекунд

        Executors.newCachedThreadPool(); // нет ограничения на количество потоков, пока не кончится память

        ExecutorService service = new ThreadPoolExecutor(10, 16, 100, TimeUnit.SECONDS, new ArrayBlockingQueue<Runnable>(15));


    }
}