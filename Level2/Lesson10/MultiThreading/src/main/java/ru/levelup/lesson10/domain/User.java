package ru.levelup.lesson10.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "user")
public class User implements Serializable {

    @Id
    private int id;
    private String login;

    @OneToOne(mappedBy = "user", fetch = FetchType.EAGER, cascade = {CascadeType.PERSIST, CascadeType.REMOVE}) // главная стала. Внешний ключ в зависимой сущности
    // eager - нашли ющера, юзеринфо сразу подтягивается. Lazy пойдет в базу еще раз когда нужна информация в user_info
    /*
    * remove
    * persist - создали user, создали user_detail, сохраняешь одно второе сразу
    * refresh - получение изменения в объект из базы. Это когда из detachted состояния переводим в персист.
    * */
    private UserDetails details;

    @OneToMany(mappedBy = "user")
    private Collection<Message> messages;

    @ManyToMany
    @JoinTable(
            name = "users_in_rooms",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "room_id")
    )
    private Collection<Room> rooms;
}
