package ru.levelup.lesson10.Dates;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;

public class DateTimeExample {
    public static void main(String[] args) {
        LocalDateTime time = LocalDateTime.of(2019, 6, 4, 19, 0, 0);
        // меняем дату, возвращается новая дата

        ZonedDateTime utc = time.atZone(ZoneId.of("UTC+3"));
        System.out.println(utc);

        LocalDateTime.now();

        ZonedDateTime inUtc1 = utc.withZoneSameInstant(ZoneId.of("UTC"));
        System.out.println(inUtc1);

        long l = time.atZone(ZoneId.systemDefault())
                .toInstant()
                .toEpochMilli();

    }
}
