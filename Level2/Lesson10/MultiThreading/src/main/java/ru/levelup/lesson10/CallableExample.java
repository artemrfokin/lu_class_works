package ru.levelup.lesson10;

import java.util.concurrent.*;

public class CallableExample {
    public static void main(String[] args) throws ExecutionException, InterruptedException, TimeoutException {
        ExecutorService executorService = Executors.newFixedThreadPool(4);
        Future<Double> future = executorService.submit(() -> {
            double value = 36345;
            return Math.sqrt(value * value / ((value + value) / 87 / 8));
        });

        future.cancel(false); // false - подожди окончения потока который еще работает. true - сразу.
        future.get(); // текущий поток блокируется
        future.get(100, TimeUnit.SECONDS); // после 100 секнж исключение. Можно обработать и идти дальше.

        int[] array = new int[1_000_000];
        ExecutorService executor = Executors.newFixedThreadPool(4);
        for (int i = 0; i < 4; i++){
            executor.submit(() -> {
//                int leftBound =
            });
        }
    }
}
