package ru.levelup.jdbc;

import java.sql.*;

public class DBHandler {
    static Connection connection = null;

    // Driver registration. Just in case.
    static {
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }


    private void connectToDB() {
        try {
            connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/Chat",
                    "postgres",
                    "root");
        } catch (SQLException e) {
            throw new RuntimeException("Connection wasn't established", e);
        }
    }


    public void updateUserData(long id, String name, String password) throws SQLException {
        if (id >= 1) {
            connectToDB();
            try (PreparedStatement preStatement = connection.prepareStatement(
                    "update users " +
                            "set login = ?, password = ? " +
                            "where id = ?")) {
                preStatement.setString(1, name);
                preStatement.setString(2, password);
                preStatement.setLong(3, id);
                preStatement.executeUpdate();
            }
            connection.close();
        } else {
            throw new IllegalArgumentException("Id has to be equals or greater than one.");
        }
    }


    public void authorisation(String login, String password) throws SQLException {
        connectToDB();
        try (PreparedStatement preStatement = connection.prepareStatement(
                "select * from users " +
                        "inner join users_info " +
                        "on users.id = users_info.user_id " +
                        "where login = ? and password = ?")) {
            preStatement.setString(1, login);
            preStatement.setString(2, password);
            ResultSet set = preStatement.executeQuery();
            if (set.next()) {
                String name = set.getString("name");
                System.out.printf("Hello, %s! You are authorized.", name);
            } else {
                System.out.println("You entered wrong login or password.");
            }
        }
        connection.close();
    }


    public void showExactUser(String login) throws SQLException {
        connectToDB();
        try (PreparedStatement preStatement = connection.prepareStatement(
                "select * from users where login = ?")) {
            preStatement.setString(1, login);
            ResultSet set = preStatement.executeQuery();
            printResults(set);
        }
        connection.close();
    }

    public void deleteUser(long id) throws SQLException {
        connectToDB();
        try (PreparedStatement preStatement = connection.prepareStatement(
                "delete from users where id = ?")) {
            preStatement.setLong(1, id);
            preStatement.executeUpdate();
        }
        connection.close();
    }


    public void addUser(String name, String password) throws SQLException {
        connectToDB();
        try (PreparedStatement preStatement = connection.prepareStatement(
                "insert into users(login, password) values (?, ?)")) {
            preStatement.setString(1, name);
            preStatement.setString(2, password);
            preStatement.executeUpdate();
        }
        connection.close();
    }


    public void showAllUsers() throws SQLException {
        connectToDB();
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery("select * from users");
            printResults(resultSet);
        }
        connection.close();
    }


    private void printResults(ResultSet set) throws SQLException {
        while (set.next()) {
            long id = set.getLong("id");
            String login = set.getString("login");
            String password = set.getString("password");
            System.out.printf("%d %s %s\n", id, login, password);
        }
    }

}
