package ru.levelup.lesson9;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Counter {
    private int value;
//    private Object = new Object();
    private Lock lock = new ReentrantLock(true);

    public synchronized int incrementAndGet(){
        return ++value;
    }

    public int get(){
        return value;
    }


        public long incrementAndGetWithSync(){
        lock.lock();
        try {
            return ++value;
        } finally {
            lock.unlock();
        }
    }

    public void unlock(){
        lock.unlock(); // при таком методе можно разблокировку можно делать вообще в другом методе. Плюс тольео отдельный блок кода проще блокировать.
    }

//    public long incrementAndGetWithSync(){ //incrementAndGet the same
//        synchronized (this){
//            return ++value;
//        }
//    }

//    public long incrementAndGetWithSync(){ //incrementAndGet the same
//        synchronized (object){
//            return ++value;
//        }
//    }

}
