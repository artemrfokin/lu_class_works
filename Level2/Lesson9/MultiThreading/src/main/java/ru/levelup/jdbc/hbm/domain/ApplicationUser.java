package ru.levelup.jdbc.hbm.domain;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "users")
@AllArgsConstructor
//@NoArgsConstructor
@Getter
@Builder // заполнение объекта не сетерами, а
// @SneakyThrows // оборачивает в try catch и кидает RunTimeException
public class ApplicationUser {
    @Id
    @GeneratedValue
    private Integer id;
    @Column(name = "login", unique = true, nullable = false)
    private String userLogin;
    private String password;

    public ApplicationUser(){};

    public String getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
