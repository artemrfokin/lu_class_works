package ru.levelup.lesson9;

public class RequestRunner implements Runnable {
    private Counter counter;

    public RequestRunner(Counter counter) {
        this.counter = counter;
    }

    @Override
    public synchronized void run() { // превратили в однопоточную программу.
        try{
            while (true){
                int i = counter.incrementAndGet();
                System.out.println(Thread.currentThread().getName() + " " + i);
                Thread.sleep(400);
            }
        } catch (InterruptedException exc){
            throw new RuntimeException(exc);
        }
    }
}
