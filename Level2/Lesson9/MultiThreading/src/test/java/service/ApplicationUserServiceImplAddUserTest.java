package service;

import org.hibernate.PropertyValueException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.hibernate.query.spi.QueryImplementor;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import ru.levelup.jdbc.hbm.domain.ApplicationUser;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class ApplicationUserServiceImplAddUserTest {
    @Mock
    private SessionFactory factory;
    @InjectMocks
    private ApplicationUserServiceImpl service;
    private Session session;
    private Transaction transaction;
    private Query query;


    @Before
    public void setUp() {
        session = Mockito.mock(Session.class);
        transaction = Mockito.mock(Transaction.class);
        Mockito.when(factory.openSession()).thenReturn(session);
        Mockito.when(session.beginTransaction()).thenReturn(transaction);
        query = Mockito.mock(QueryImplementor.class);
    }


    @After
    public void tearDown(){
    }


    /* //              addUser Tests            // */

    @Test(expected = PropertyValueException.class)
    public void testAddUser_LoginIsNull_ThrowsException() {
        service.addUser(null, "value");
    }


    @Test(expected = PropertyValueException.class)
    public void testAddUser_PasswordIsNull_ThrowsException() {
        service.addUser("value", null);
    }


    @Test
    public void testAddUser_WhetherApplicationUserSavedWithProperArguments() {
        String login = "value1";
        String password = "value2";
        service.addUser(login, password);
        ArgumentCaptor<ApplicationUser> argument = ArgumentCaptor.forClass(ApplicationUser.class);
        Mockito.verify(session).save(argument.capture());
        Assert.assertSame(login, argument.getValue().getUserLogin());
        Assert.assertSame(password, argument.getValue().getPassword());
    }


    @Test
    public void testAddUser_TransactionAndSessionClosingDone() {
        service.addUser("value1", "value2");
        Mockito.verify(transaction).commit();
        Mockito.verify(session).close();
    }



    /* //           updateUserInfo Test            // */

    // Input data tests
    @Test(expected = PropertyValueException.class)
    public void testUpdateUserInfo_idIsNegative_throwsException(){
        service.updateUserInfo(-1, "value", "value2");
    }

    @Test(expected = PropertyValueException.class)
    public void testUpdateUserInfo_idIsZero_throwsException(){
        service.updateUserInfo(0, "value", "value2");
    }

    @Test(expected = PropertyValueException.class)
    public void testUpdateUserInfo_loginIsNull_throwsException(){
        service.updateUserInfo(3, null, "value2");
    }

    @Test(expected = PropertyValueException.class)
    public void testUpdateUserInfo_passwordIsNull_throwsException(){
        service.updateUserInfo(3, "value", null);
    }

    // Shit test which doesn't take in account id
    @Test
    public void testUpdateUserInfo_verifySessionPersistWithAppUser(){
        Integer id = 4;
        String login = "value1";
        String password = "value2";
        Mockito.when(session.get(Mockito.eq(ApplicationUser.class), Mockito.anyInt()))
                .thenReturn(new ApplicationUser());
        service.updateUserInfo(id, login, password);
        ArgumentCaptor<ApplicationUser> argCaptor = ArgumentCaptor.forClass(ApplicationUser.class);
        Mockito.verify(session).persist(argCaptor.capture());
//        Assert.assertSame(id, argCaptor.getValue().getId());
        Assert.assertSame(login, argCaptor.getValue().getUserLogin());
        Assert.assertSame(password, argCaptor.getValue().getPassword());
        /*передай юзера уже готового и не проверяй сетеры
        * А по хорошему вообще create и update возвращает результат. Его и проверяешь.
        * @Con
        * */
    }

//    @Test
//    public void testUpdate_validParams(){
//        int id = 10;
//        String login = "login";
//        String password = "pasword";
//
//    }


    @Test
    public void testUpdateUserInfo_TransactionAndSessionClosingDone() {
        service.updateUserInfo(1,"value1", "value2");
        Mockito.verify(transaction).commit();
        Mockito.verify(session).close();
    }


    /* //          showAllUser Test - nothing to test        // */


    /* //               deleteUser Tests                    // */
    @Test(expected = PropertyValueException.class)
    public void testDeleteUser_idIsZero_throwsException(){
        service.deleteUser(0);
    }

    @Test(expected = PropertyValueException.class)
    public void testDeleteUser_idIsNegative_throwsException(){
        service.deleteUser(-1);
    }


    @Test
    public void testDeleteUser_UsageSetParameterMethodWithRightParameters(){
        Mockito.when(session.createQuery(Mockito.eq("delete ApplicationUser where id = :id")))
                .thenReturn(query);
        int id = 4;
        service.deleteUser(id);
        Mockito.verify(query).setParameter(Mockito.eq("id"), Mockito.eq(id));
    }

    @Test
    public void testDeleteUser_UsageExecuteUpdateMethod(){
        service.deleteUser(4);
        Mockito.verify(query).executeUpdate();
    }

    @Test
    public void testDeleteUser_ConnectionClose(){
        service.deleteUser(4);
        Mockito.verify(transaction).commit();
        Mockito.verify(session).close();
    }


    /* //          .authorisation Tests          //*/
    @Test(expected = PropertyValueException.class)
    public void testAuthorisationTest_loginIsNull_throwsExceptoin(){
        service.authorisation(null, "value2");
    }


    @Test(expected = PropertyValueException.class)
    public void testAuthorisationTest_passwordIsNull_throwsExceptoin(){
        service.authorisation("value", null);
    }


    @Test
    public void testAuthorisation_QueryWithRightParameters(){
        Mockito.when(session.createQuery(
                                        Mockito.eq("from ApplicationUser" +
                                                " where userLogin= :login and password = :password"))
                                        ).thenReturn(query);

        String login = "value1";
        String password = "value2";
        service.authorisation(login, password);

        Mockito.verify(query).setParameter(Mockito.eq("login"), Mockito.eq(login));
        Mockito.verify(query).setParameter(Mockito.eq("password"), Mockito.eq(password));
    }


    @Test
    public void testAuthorisation_ConnectionClosing(){
        Mockito.when(session.createQuery(Mockito.anyString())).thenReturn(query);

        service.authorisation("value1", "value2");

        Mockito.verify(transaction).commit();
        Mockito.verify(session).close();
    }

}