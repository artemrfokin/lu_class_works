package ru.levelup.jdbc;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.runners.MockitoJUnitRunner;

import java.sql.SQLException;

@RunWith(MockitoJUnitRunner.class)
public class DBHandlerTest {

    private DBHandler dbHandler = new DBHandler();

    @Test(expected = IllegalArgumentException.class)
    public void testUpdateUserData_idIsNegative_throwsException() throws SQLException {
        dbHandler.updateUserData(-5L, "cc", "ca");
    }
}
