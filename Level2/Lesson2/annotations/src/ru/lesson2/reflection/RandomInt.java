package ru.lesson2.reflection;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME) // на каком этаме удаляется аннотация
// @override - source аннотация. аннотация - метка.
// обчно
@Target({ElementType.FIELD
        // Type
})
public @interface RandomInt {
    int min();
    int max();

}
