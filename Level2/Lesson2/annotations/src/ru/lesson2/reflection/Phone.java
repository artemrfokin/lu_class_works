package ru.lesson2.reflection;

public class Phone {
    private String name;
    @RandomInt(min = 1, max = 12)
    private int coreNumber;

    public Phone(String name, int coreNumber){
        this.coreNumber = coreNumber;
        this.name = name;
    }

    private Phone(){};
}