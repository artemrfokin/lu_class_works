package ru.lesson2.reflection;

import java.lang.reflect.Field;
import java.util.Random;

public class RandomIntProcessor {
    public static Phone process(Phone phone) throws IllegalAccessException {
        Class classOfPhone = phone.getClass();
        Field[] fields = classOfPhone.getDeclaredFields();
        for (Field field : fields){
            RandomInt annotation = field.getAnnotation(RandomInt.class);
            if (annotation != null){
                Random random = new Random();
                int randomInt = random.nextInt(annotation.max() - annotation.min() + 1 + annotation.min());
                field.setAccessible(true);
                field.set(phone, randomInt);
            }
        }
        return phone;
    }
}
