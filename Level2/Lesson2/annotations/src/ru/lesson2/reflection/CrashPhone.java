package ru.lesson2.reflection;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;

public class CrashPhone {
    public static void main(String[] args) {
        /*
        * Получение объекта класса Class. Первый способ. Из объекта.
        * */
        Phone firstPhone = new Phone("iPhone", 4);
        Class classOfPhone = firstPhone.getClass();

        // без объекта. Прямо от класса.
        Class literalClassPhone = Phone.class;

        Field[] fields = classOfPhone.getDeclaredFields(); // каждое поле для класса публичны
        Arrays.stream(fields).forEach(field -> { // переобределяем метод accept()
            System.out.println(field.getName() + ", type: " + field.getType().getName());
        });

        Constructor[] constructors = classOfPhone.getDeclaredConstructors();
        Constructor withOutParametors = Arrays.stream(constructors)
            .filter(constructor -> constructor.getParameterCount() == 0)
                .findFirst()
                .get();

        try {
            withOutParametors.setAccessible(true); // каждый раз у каждого объекта такую штуку писать надо.
            Phone createdPhone = (Phone) withOutParametors.newInstance();
            System.out.println(createdPhone != null);
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        // рефлексия просто очень долго работает прямой метод 1 млс, рефлексия 800 млс.
    }
}
