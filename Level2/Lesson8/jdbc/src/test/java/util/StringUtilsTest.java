package util;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

public class StringUtilsTest {
    // when then
    // _input_return
    @Test
//    @Ignore
    public void testIsEmpty_valueIsNull_returnTrue(){
        // groovy Spok
        //given входнык
        String value = null;
        // when
        boolean result = StringUtils.isEmpty(value);
        // then
        // будет ошибка, когда не true
//        assert !result;
        Assert.assertTrue(result);
    }
    @Test
    public void testIsEmpty_valueIsNotEmpty_returnFalse(){
        String value = "value";
        boolean result = StringUtils.isEmpty(value);
        Assert.assertFalse(result);
    }
}
