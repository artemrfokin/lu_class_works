package service;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class) // инициализирует все Моки
public class ApplicationUserServiceImplTest {
    @Mock
    private SessionFactory factory;
    private Session session;
    private Transaction transaction;

    @InjectMocks // работает с Моками которые созданы выше. Смотрит на конструктор
    private ApplicationUserServiceImpl service;

    @Test
    public void testCreateUser(){
        service.createUser("efef", "efef");
//        Mockito.verify(transaction, Mockito.times(2)).commit(); // на случай если цикл был, то и закрытий какое количество
        Mockito.verify(transaction).commit();
        Mockito.verify(session).close();
    }

    // before - перед каждым тестовым методом
    // beforeClass - перед вообще всем тестом
    // after в итеграционный тест
    // afterClass в

    @Before // отработает перед каждым тестовым методом
    public void setUp(){
        session = Mockito.mock(Session.class); // создали класс, объект в байткоде
        transaction = Mockito.mock(Transaction.class);
        Mockito.when(factory.openSession()).thenReturn(session);
        Mockito.when(session.beginTransaction()).thenReturn(transaction);

    }

}