package ru.levelup.jdbc.hbm;

import org.hibernate.SessionFactory; // берем только из этого пакета
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration; // onlu this package
import org.hibernate.service.ServiceRegistry;


public class SessionFactoryInitializer {
    private static SessionFactory factory;

    static {
        Configuration configuration = new Configuration().configure(); // автоматом идет в корень resources
//        ServiceRegistry registry = new StandardServiceRegistryBuilder().build(); // доп настройки
        factory = configuration.buildSessionFactory();
    }

    public static SessionFactory getFactory(){
        return factory;
    }
}
