package ru.levelup.jdbc.hbm.domain;

import javax.persistence.*;

@Entity
@Table(name = "users") // название таблицы в базе
public class ApplicationUser {
    @Id
    @GeneratedValue // сам заполни ID
    private Integer id;
    @Column(name = "login", unique = true, nullable = false) // нельзя поля писать через подчеркивание
    private String userLogin;
    private String password;

    public String getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
