package org.levelup.server.chat.database;

import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;
import org.levelup.server.chat.domain.Room;

import java.util.Properties;

public class SessionFactoryinitioalizer {
    private static SessionFactory factory;

    static {
        Properties dbProperies = new Properties(); // потокобезопасная HashMap
        dbProperies.setProperty("hibernate.connection.driver_class", "org.postgresql.Driver");
        dbProperies.setProperty("hibernate.connection.url", "jdbc:postgresql://localhost:5432/ChatTest");
        dbProperies.setProperty("hibernate.connection.username", "postgres");
        dbProperies.setProperty("hibernate.connection.password", "root");
        dbProperies.setProperty("hibernate.hbm2ddl.auto", "create");
        dbProperies.setProperty("hibernate.show_sql", "true");
        dbProperies.setProperty("hibernate.format_sql", "true");
        dbProperies.setProperty("hibernate.dialect", "org.hibernate.dialect.PostgreSQL95Dialect");
        dbProperies.setProperty(Environment.CACHE_REGION_FACTORY, "org.hibernate.cache.ehcache.EhCacheRegionFactory");
        dbProperies.put(Environment.USE_SECOND_LEVEL_CACHE, true);
        dbProperies.put(Environment.USE_QUERY_CACHE, true);

        StandardServiceRegistry registry = new StandardServiceRegistryBuilder()
                .applySettings(dbProperies).build();
        factory = new Configuration()
                .addAnnotatedClass(Room.class) // забавное добавление
                .buildSessionFactory(registry);
    }

    public static SessionFactory getFactory() {
        return factory;
    }

    /*
    * команды разные для комнат
    * юзер может сам запрашивать список комнат.
    *
    * */
}
