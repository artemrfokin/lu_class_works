package org.levelup.server.chat.repositary;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.levelup.server.chat.domain.Room;
import org.levelup.server.chat.database.SessionFactoryinitioalizer;

import java.util.Collection;
import java.util.List;

public class RoomRepositaryImpl implements RoomRepository{
    private SessionFactory factory;

    public RoomRepositaryImpl() {
        this.factory = SessionFactoryinitioalizer.getFactory();
    }

    @Override
    public Room createRoom(String name) {
        Session session = factory.openSession();
        Transaction transaction = session.beginTransaction();

        Room room = new Room("Java");
        session.save(room);

        transaction.commit();
        session.close();

        return room;
    }


    @Override
    public Collection<Room> findAll() {
        Session session = factory.openSession();
        Transaction transaction = session.beginTransaction();

        Query query = session.createQuery("from Room", Room.class);
        List<Room> list = query.list();

        transaction.commit();
        session.close();

        return list;
    }
    /*
    * DAO (Data Access Object)- общение с дабой/ CRUD (всегда должно быть 5 методов) Но искать иногда надо еще и по полям искать
    * Services - обработка / вызов
    * Repositories - на современном уровне DAO + поиск по полям.
    * */
}
