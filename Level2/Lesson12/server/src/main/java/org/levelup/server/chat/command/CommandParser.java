package org.levelup.server.chat.command;

import java.util.HashMap;
import java.util.Map;


public class CommandParser {
    private Map<String, CommandExecutor> executors;

    {
        executors = new HashMap<>();
        executors.put("rooms", new GetRoomsCommandExecutor());
    }


    public void executeCommand(String line) {
        CommandExecutor executor = executors.get(line);
        if (executor != null){
            executor.execute();
        } else {
            System.out.println("Entered wrong command");
            // см код димы тут интересно Optioanl
        }
    }
}
