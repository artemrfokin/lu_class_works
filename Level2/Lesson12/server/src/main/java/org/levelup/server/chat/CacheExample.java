package org.levelup.server.chat;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.levelup.server.chat.database.SessionFactoryinitioalizer;
import org.levelup.server.chat.domain.Room;

public class CacheExample {
    public static void main(String[] args) {
        SessionFactory factory = SessionFactoryinitioalizer.getFactory();
        Session session = factory.openSession();
        session.beginTransaction();

        Room r = new Room("Room #1");
        session.persist(r);

        session.getTransaction().commit();
        session.close();

        // /////
        session = factory.openSession();
        session.beginTransaction();

        session.get(Room.class, r.getId());

        session.getTransaction().commit();
        session.close();

        //////
        session = factory.openSession();
        session.beginTransaction();

        session.createQuery("from Room")
                .setCacheable(true)
                .getResultList();

        session.getTransaction().commit();
        session.close();

        //////
        session = factory.openSession();
        session.beginTransaction();

        session.createQuery("from Room")
                .setCacheable(true)
                .getResultList();

        session.getTransaction().commit();
        session.close();


        ////
        session = factory.openSession();
        session.beginTransaction();

        for (int i = 0; i < 1000; i++){

            session.persist(new Room("room " + i));
            if (i % 20 == 0){
                session.flush();
                session.clear();
                session.getTransaction().commit();
                session.beginTransaction();
            }
        }
        session.getTransaction().commit();
        factory.close();
    }
}
