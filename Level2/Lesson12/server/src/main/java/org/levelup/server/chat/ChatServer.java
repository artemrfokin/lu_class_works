package org.levelup.server.chat;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.concurrent.ThreadPoolExecutor;


public class ChatServer {
    private final int port;
    private ClientListener listener;

    public ChatServer(int port) {
        this.port = port;
    }

    public void startServer() throws IOException {
        ServerSocket serverSocket = new ServerSocket(port);
        listener = new ClientListener(serverSocket);
        new Thread(listener).start();
        new Thread(new CommandWorker(this)).start();
    }

    public void stopServer() throws IOException {
        listener.shutDown();
    }

    // lobok stavim otdelno
}
