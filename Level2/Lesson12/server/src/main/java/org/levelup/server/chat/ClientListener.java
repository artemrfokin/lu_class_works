package org.levelup.server.chat;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;


public class ClientListener implements Runnable {
    private final ServerSocket server;
    private volatile boolean running = true;

    public ClientListener(ServerSocket server) {
        this.server = server;
    }

    @Override
    public void run() {
        while (running){
            try {server.accept();
            }
            catch (IOException ignored){
            }
        }
    }
    // main умрет есть его логика закочена

    public void shutDown() {
        try{
            running = false;
            server.close();
        }
        catch (IOException exc){
            System.out.println("Server stopped..");
        }
    }
}
