package ru.levelup.jdbc.hbm;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import ru.levelup.jdbc.hbm.domain.ApplicationUser;

public class Main {
    public static void main(String[] args) {
        SessionFactory factory = SessionFactoryInitializer.getFactory();
        ApplicationUser user = new ApplicationUser();
        user.setUserLogin("JavaLogin");
        user.setPassword("qwerty");
        Session session = factory.openSession();

        Transaction t = session.beginTransaction();
        Integer id = (Integer) session.save(user);
        System.out.println("id " + id);
        session.flush();

        user.setPassword("new password");
        t.commit();
        session.close();

        user.setPassword("new new password");
        factory.close();
    }
}
