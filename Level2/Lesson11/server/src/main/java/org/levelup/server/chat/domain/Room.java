package org.levelup.server.chat.domain;

import javax.persistence.*;

@Entity
@Table(name = "rooms")
public class Room {
    @Id
    @SequenceGenerator(name = "room_id_generator",
            sequenceName = "room_id_seq",
    initialValue = 10000
    ) // room_id_seq название отдельной таблицы где будут генерироваться новые айди.
    @GeneratedValue(generator = "room_id_generator", strategy = GenerationType.SEQUENCE)
    private int id;

    @Column(unique = true, nullable = false)
    private String name;

    public Room() {}

    public Room(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
