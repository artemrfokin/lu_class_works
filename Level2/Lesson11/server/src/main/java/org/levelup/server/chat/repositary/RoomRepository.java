package org.levelup.server.chat.repositary;

import org.levelup.server.chat.domain.Room;

import java.util.Collection;

public interface RoomRepository {

    Room createRoom(String name); // всегда должен возвращать объект. Но смотри надо или нет.

    Collection<Room> findAll();
}
