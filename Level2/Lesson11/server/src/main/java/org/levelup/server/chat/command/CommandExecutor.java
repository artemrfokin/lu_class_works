package org.levelup.server.chat.command;

public interface CommandExecutor {
    public void execute();
}
