package org.org.levelup.threads;

public class ThreadExample {
    public static void main(String[] args) {
        // Thread
        // Runnable (можно конструкто не использовать)
        // Callable // не void должна что то вернуть
        Thread thread = new Thread();
        thread.start(); //тут работает 2 потока main и второй

//        Thread mythread = new MyThread();
//        mythread.setDaemon(true); // фоновые поток. Его завершения не будет никто ждать.
        // Прогресс бар.
//        mythread.start();
        // main умер но mythread жив.

//        for (int i = 0; i < 1000; i++) {
//            System.out.println(i * i);
//        }
        Thread fromRunnable = new Thread(new MyRunnable());
//        MyRunnable myRunnable = new MyRunnable();
//        myRunnable.run(); // выполнение в текщем потоке
        fromRunnable.run();
//        fromRunnable.stop(); // жесткая остановка. В промежуточном состоянии не закрываем никогда.
        // нужно просто удалить все что наделал.

        /*
        * Лучше интерфейс чем класс Thread
        * ткт не занимаем extends
        *
        * */
    }

//    public static class MyThread extends Thread {
//        @Override
//        public void run() {
//            System.out.println("Before sleep");
//            try {
//                Thread.sleep(3000);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//            System.out.println("Message from my thread.");;
//        } // как только доходит до конца поток удалится.
//    }

    public static class MyRunnable implements Runnable{
        @Override
        public void run() {
            System.out.println("Program will finish in 5 seconds");
            for (int i = 5; i > 0; i--){
                try {
                    Thread.sleep(1000);
                    System.out.println(i);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
