package org.levelup.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
    private static int clients = 0;
    private static boolean haveToListen = true;

    private Server() {
    }

    public static void minusClent() {
        clients--;
    }
    public static void plusClient(){
        clients++;
    }
    public static int getClients(){
        return clients;
    }

    public static void startServer(int port) {
        /*
         * */
        try {
            ServerSocket server = new ServerSocket(port); // создаем слушателя
            //thread-per-client - не очень хорошо, тк память кончится.
            // И вообще выделение потока это долгая операция. Не критично.
            // one-thread (один всех клиентов) - медленно
            // pull -thread (сами на старте делаем 10 шт. В боулинге 10 ботинок. либо
            // увеличивает потоки либо клиенты ждут)

            while (true) {
                Socket client = server.accept(); // объект клиента на нашей стороне. Он блокирующий.
                // Сервер (поток) встал пока клиент к нам не пришел.
                Thread clientThread = new Thread(new ClientWorker(client));
                clientThread.start();
            }
        }
        catch(
    IOException exc)

    {
        exc.printStackTrace();
    }
}
}
