package org.levelup.server;

import java.io.*;
import java.net.Socket;

public class ClientWorker implements Runnable {
    private Socket client;
    public ClientWorker(Socket client){
        this.client = client;
    }

    @Override
    public void run() {
//        client.getInputStream() // то что нам пишет клиент
//        client.getOutputStream() // то что отдаем

        try (BufferedReader fromClient = new BufferedReader(new InputStreamReader(client.getInputStream()));
             BufferedWriter toClient = new BufferedWriter(new OutputStreamWriter(client.getOutputStream()))
        ){
            Server.plusClient();
            System.out.println("Plus one client. Number of clients " + Server.getClients());
            String clientMessage = "";
            while (!clientMessage.equals("exit")){
                clientMessage = fromClient.readLine();
                toClient.write("Hello " + clientMessage + "\n");
                toClient.flush(); // очищаем буфер
            }
            System.out.println("Worker deleted");
            Server.minusClent();
            System.out.println("Number of clients " + Server.getClients());
        } catch (IOException exc){
            exc.printStackTrace();
        }
    }
}
