package org.levelup.client;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

public class Client {
    public static void main(String[] args) throws IOException {
        Socket socket = new Socket(InetAddress.getByName("localhost"), 7878);
        BufferedReader consoleReader = new BufferedReader(new InputStreamReader(System.in));
        BufferedReader fromServer = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        BufferedWriter toServer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));

        String inputString = "";
        while (!inputString.equals("exit")) {
            System.out.println("Enter string");
            inputString = consoleReader.readLine();
            toServer.write(inputString + "\n");
            toServer.flush();

            String stringFromServer = fromServer.readLine();
            System.out.println("Server answer: " + stringFromServer);
        }
    }
}
