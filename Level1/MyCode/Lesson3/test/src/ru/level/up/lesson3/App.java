package ru.level.up.lesson3;

public class App { // public - виден отовсюду
    public static void main(String[] args) {
        // ссылка = reference = object = экземпляр
        // пустой байт 16 байт
        // int 4 , но java округляет до 8 = 24
        // суммируем потом доводим до 8 кратного.
        // boulean - 4 байта как отдельная переменная. В массиве 1 бит.

        Book firstBook = new Book(); // только после написания new выделяется оперативная память.
        firstBook.weigth = 500d;
        firstBook.price = 100;
        firstBook.discount = 30;
        double firstBookFinalPrice = firstBook.calculatePrice();
//        System.out.println("Weight = " + firstBook.weigth);

        Book secondBook = new Book(); // вызов конструктора. инициализация полей
        //
        secondBook.name = "Flash MX 2204"; // shift+alt + стрелки;
        secondBook.weigth = 230.44d;
        secondBook.price = 596.4d;
        secondBook.discount = 32;

//        System.out.println("Name of firstBook" + firstBook.name); // null ссылка в некуда. NullPointerException.
//        System.out.println("Name of second book = " + secondBook.name);
//        System.out.println("Weight = " + secondBook.weigth);

//        System.out.println("Final price = " + firstBookFinalPrice);
//        System.out.println("Final price of second: " + secondBook.calculatePrice());
//        System.out.println("Final price with discount = " + secondBook.calculatePrice(20));
        //dry - dont repeat yourself

        Book thirdBook = new Book("Бла-бла-бла", 150);
//        System.out.printf("Third book is = %s и цена %f", thirdBook.name, thirdBook.price);
        thirdBook.print();

    }
}
