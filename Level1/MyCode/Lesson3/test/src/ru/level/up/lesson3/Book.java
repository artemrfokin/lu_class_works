package ru.level.up.lesson3;

public class Book {
    // Порядок поля, конструкторы, методы.

    double weigth; // поля класса, field
    String name;
    double price; // не надо инициализировать!)
    int discount;


    public double getPrice(){
        return price;
    }

    public void setPrice(double price){
        this.price = price;
    }

    // передавая в функцию примитив и менябб переменную внутри функции - это ничего не поменяет.
    //Не смей делать валидацию в сеттере.

    /*
    * private - поле/ класс /метод / конструктор не могут быть вызваны вне пределов класса
    * default-package (private-package) - выставляется по умоланию. Доступ будет
    * ограничение на видисмость в пределах класса или пакетавнутри пакета
    * protected -
    * public - доступ открыт всем (применяется ) для класса .
    * */

    Book(){
        name = "Без названия";

    }

    Book(String bookName){
        name = bookName;
    }

    Book(String name, double price){
        this.name = name;
        this.price = price;
    }
    // сигнатура метода = название + набор входных параметров (как тип так и порядок аргументов)
    double calculatePrice(){
//        double finalPrice = price - price * discount / 100;
        return calculate(0);
    }

    // перегрузка метода = method overloading = несколько методов с одиноковым названием но сигнатура разная.
    double calculatePrice(int personDiscount){
        return calculatePrice() * personDiscount / 100;
    }

    private double calculate(int personDiscount){
        return price - price * discount / 100;
    }

    void print(){
        System.out.printf("Third book is = %s и цена %.2f", name, price);
    }
}
