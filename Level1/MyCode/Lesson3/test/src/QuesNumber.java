import java.util.Random;
import java.util.Scanner;
// alt + enter на красной надписи
// jdk/rt

public class QuesNumber {
    public static void main(String[] args) {
        //psvm
        //sout
        while (true) {
            Scanner sc = new Scanner(System.in);
            System.out.println("Введите число");
            int number = sc.nextInt(); // Scanner всегда строку изначально потом преобразовывает.

            Random rnd = new Random();
            // случайные числа формируются только специальной железкой
            int secretNumber = rnd.nextInt(7); // по дефолту от 0 до 32

            if (number == secretNumber) {
                System.out.println("Вы угадали!");
            } else if (number > secretNumber){
                System.out.println("Ваше число больше" + " чем " + secretNumber);
            } else if (number < secretNumber){
                System.out.println("Ваше число меньше" + " чем " + secretNumber);
            }
        } // cntr alt l - форматирование отсутпов и переносов
    }
}
