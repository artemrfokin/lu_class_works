package lesson5.static_classes;

public class StaticTask {
    public static void main(String[] args) {
        StaticTask task = null;
        /*
        * конференция joker, highload;
        *
        * */
        task.print(); // компилятор уберет имя объекта  и подставит просто имя класса.
    }

    private static void print(){
        System.out.println("Print");
    }
}
