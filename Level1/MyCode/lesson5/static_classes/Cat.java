package lesson5.static_classes;

public class Cat {
    private double weight;
    private int age;
    private String name;


    private Cat(double weight, int age, String name) { // теперь создавать объекты можно создаваться только через
        // createInstance

        this.weight = weight;
        this.age = age;
        this.name = name;
    }

    //Статический инициализатор объекта
    public static Cat createInstance(String name, double weight, int age){
        // рабочий вариант для параментров меньше 4х
        if (StringUtil.isEmplty(name) || age < 0 || weight < 0){
            return null;
        }
        return new Cat(weight, age, name);
    }

    @Override
    public String toString() {
        return "Cat{" +
                "weight=" + weight +
                ", age=" + age +
                ", name='" + name + '\'' +
                '}';
    }
    /*
    * стринг пул просто String s
    * Отдельный объект тnew String();
    * Строка - неизменяемая. Любое изменение - создание новой строки. Явно перезаписываем или возврашаем.
    * String s2 = "q" + "w" + "e" + "r" + "t" + "y"
    * */
}
