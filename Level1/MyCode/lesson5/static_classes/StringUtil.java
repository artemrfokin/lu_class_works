package lesson5.static_classes;

public class StringUtil {

    public static void main(String[] args) {
        System.out.println(isEmplty(null));
        System.out.println(isEmplty(""));
        System.out.println(isEmplty("     "));
        System.out.println(isEmplty("Some text"));
    }
    // null, '' '   '
    // tirm -  убераме пробелы в начале и конче
    public static boolean isEmplty(String value){
//        if (value == null) return true;
        return value == null || value.trim().isEmpty();
    }
    /*
    * java приззагрузке ищет все что на static  и сразу инициализируется, выделяется память.
    * unit test - тест отдоного метода или класса. Заглушки - объекс пустыми методами.
    * Статику нельзя переписать следовательно даже если в двух классах будут методы с одинковым названием
    * В памяти будет два метода.
    * Назначение автотестов  - перепроверять 100 классов после изменения 2х.
    * Объект протиа статики  - когда не надо хранить данные.
    * */
}
