package lesson5.static_classes;

public class App {
    public static void main(String[] args) {
        StaticExample e = new StaticExample();
        e.field = 10;
        e.staticfield = 20;

        StaticExample e2 = new StaticExample();
        e2.field = 15;
        e2.staticfield = 25;

        System.out.println(e2.staticfield);
    }
}
