package lesson5.static_classes;

public class Main {
    public static void main(String[] args) {
        Cat c1 = Cat.createInstance("Vasya", 2, 4);
        Cat c2 = Cat.createInstance("", 2, 4);
        System.out.println(c1);
        System.out.println(c2);
    }
}
