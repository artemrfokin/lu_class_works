package lesson5.person;

public interface Shiftable {
    public static final int START_WORK = 10; // final - неизменяемо UPPER_CASE обязателен
    public static final int END_WORK = 18; // public static final все три обязательно в интерфесе

    // конструктор отсутствует всегда т.к. все абсолютно методы абстрактны.
    // проверка работатет ли в этот день человек
    public abstract boolean isAtWork(int hour); // public abstract всегда у интерфейса
}
