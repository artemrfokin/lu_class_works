package lesson5.person;

// если есть абс метод, то класс обязан быть абс
// но класс может быть абстракным без абс методов.
// нельзя создавать собъекты Profession только по new конструктору
public abstract class Profession {
    protected String name; // внутри класса, пакета, наследниках

    protected Profession(String name){
        this.name = name;
    }

    public abstract void job();
    /*
    * интерфес - класс со всеми абс методами
    * класс - существительное, метод - глагол, интерфейс - прилагательное. хуйня.
    * Назначение - множественное наследование.
    *
    * */
}
