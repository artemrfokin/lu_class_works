package lesson5.person;

public class Application {
    public static void main(String[] args) {
        Profession qa = new QA();
        Profession analyst = new Analyst();
        Profession developer = new Developer();

        Profession[] professions = {qa, analyst, developer};

        for (int i = 0; i < professions.length; i++){
            System.out.println(professions[i].name);
            professions[i].job();
            System.out.println("");
        }

        Shiftable shiftable = new Analyst();
        shiftable.isAtWork(15);

        Analyst an = new Analyst();
        an.compareTo(developer);
    }
}
