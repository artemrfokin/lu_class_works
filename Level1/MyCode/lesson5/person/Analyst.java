package lesson5.person;

public class Analyst extends Profession implements Shiftable, Comparable { // с помощью интерфейсов можно в единичный
    // класс добавить функционал.
    // в жизни класс, интерфейсы, может быть наследование.
    public Analyst(){
        super("Business analyst");
    }

    @Override
    public void job() {
        System.out.println("Write TT");
        System.out.println("Drink tea");
        System.out.println("Make diagram");
    }

    @Override
    public boolean isAtWork(int hour) {
        return hour >= START_WORK && hour <= END_WORK;
    }

    @Override
    public int compareTo(Object o) {
        return 0;
    }
}
