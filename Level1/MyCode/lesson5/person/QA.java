package lesson5.person;


// alt enter - вызвать быстрое переделине.
public class QA extends Profession {

    public QA(){
        super("Quality Assurance");
    }

    @Override
    public void job() {
        System.out.println("Make test");
        System.out.println("Report");
    }
}
