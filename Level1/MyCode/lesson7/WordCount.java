package lesson7;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class WordCount {
    public static void main(String[] args) {
        String words = "What is Lorem Ipsum?\n" +
                "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.";

        words.replace(",", "").replace(".", "").replace("\n", " ").replace("!", "").replace("?", "").replace("\"", "");
        String[] allWords = words.split(" ");


        Map<String, Integer> map = new HashMap<>();
        for (int i = 0; i < allWords.length; i++ ){
            if (allWords[i] == null || allWords[i].trim().isEmpty()){
                continue;
            }
            String w = allWords[i].toLowerCase();
            Integer count = map.get(w);
            if (count == null){
                map.put(w, 1);
            } else {
                map.put(w, count + 1);
            }
        }
        // способы вывода
//        map.keySet(); // дость
//        Set<String> keys = map.keySet();
//        for (String key : keys){
//            Integer count = map.get(key);
//            System.out.println(key + " " + count);
//        }
//        Set<Map.Entry<String, Integer>> entries = map.entrySet();
//        for (Map.Entry<String, Integer> entry : entries){
//            System.out.println(entry.getKey() + " " + entry.getValue());
//        }
//
        map.forEach((k, v) -> System.out.println(k + " " + v));
        // не удаляй никогда жлементы в for each

    }
}
