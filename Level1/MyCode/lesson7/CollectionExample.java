package lesson7;

import java.util.*;

public class CollectionExample {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>(); // toString работает только
        // с типом String; можешь менять на LinkedList
        list.add("String");
        list.add("S1");
        list.add("pull");
        list.add("push");
//        System.out.println(list.toString());
//        System.out.println("Index of pull" + list.indexOf("pull")); // если хочешь что
        // искать потом метод equals надо переопрееделять
//        System.out.println("Index of pull: " + list.indexOf("pu"));

        List<String> subList = list.subList(1, 2);
//        System.out.println(subList);
//        Map<String,Integer> map = new HashMap<>()Map<String, Integer>();
        // для сравнения внедряется интерфейс Comparable и реализуем compareTo
        // Или внедряем Comparator

        Iterator<String> iterator = list.iterator();
        while (iterator.hasNext()){
            String el = iterator.next();
            if (el.length() > 3){
                iterator.remove();
            }
        }
    }
}
