package Lesson6.Structure;

//int
public interface Structure {
    void add(int value);
    boolean isEmpty();
    int size();
    // remove(); System.arrayCopy()
}
