package Lesson6.Structure;

import lesson4.ElectronicBook;

public class Element {
    private int value;
    private Element next;

    public Element(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public void setNext(Element element) {
        this.next = element;
    }

    public Element getNext() {
        return next;
    }
}

//    @Override
//    public void add(int value) {
//
//    }
//
//    @Override
//    public boolean isEmpty() {
//        return false;
//    }
//
//    @Override
//    public int size() {
//        return 0;
//    }
//}
