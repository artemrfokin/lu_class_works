package Lesson6.Structure;

public class List implements Structure{
    private int size;
    private Element head;

    @Override
    public void add(int value) {
        /*
        * 1. Head == null
        * 2-1 heag == записываем элеменит в head
        * 2-2 ищем последний (у которого next == null)
        *
        * как ищем
        *while next != null
        * */
        Element el = new Element(value);
        if (head == null){
            head = el;
        }
        else {
            Element current = head;
            while (current.getNext() != null){
                current = current.getNext();
            }
            current.setNext(el);
        }
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public int size() {
        return 0;
    }
    /*
    * Однонапраяленный связный список
    * Head - первый элемент
    * элемент - отдельный класс (fields next, value);
    * последний элемент  - у которого next = null.
    * Head можно перекидывать на другой элеменит, чтобы добавлять элемент в начало
    * */
}
