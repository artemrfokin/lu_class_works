package Lesson6.Structure;

// список на основе массива
public class DynamicArray implements Structure{
    /*
    *
    * добавление сложнее
    * ArrayList работает быстрее на практике LinkedList;
    * ArrayList (хранить - и все ) быстрее LinkedList (ОЧЕРЕДЬ и стек)
    * всего две структуру данных массив(пласт) и список(хаотично).
    * // распарсить xml
    * Вынести
    * generic method параметризированный метод
    * generiki работают только с классами-обертками.
    * */

    private int[] array;
    private int size; // количество элементов в стурктуре

    public DynamicArray(int initialCapacity){
        if (initialCapacity <= 0){
            initialCapacity = 10;
        }
        this.array = new int[initialCapacity];
    }

    @Override
    public void add(int value) {
        /*
        * есть ли место
        * нет - создаме * 1,5
        * копируем старый в новый
        * увеличиваем size
        * */
        if (array.length == size){
            // нужна просто новая ссылка память никуда не денется
            int[] oldArray = array;
            array = new int[(int) (size * 1.5d)];
            System.arraycopy(oldArray, 0, array, 0, size); // и с динамическими сработает.
        }
        array[size++] = value;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public int size() {
        return size;
    }
}
