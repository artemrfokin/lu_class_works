package Lesson6.Structure;

public abstract class AbstractList<T> {
    public abstract void add(T value);

    public abstract void remove(int index);

    public abstract int get(int index);

    public abstract boolean isEmpty();

    public abstract int size();


}
