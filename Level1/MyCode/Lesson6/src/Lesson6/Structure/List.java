package Lesson6.Structure;

public class List implements Structure {
    private int size;
    private Element head;

    @Override
    public void add(Object value) {
        Element el = new Element(value);
        if (head == null) {
            head = el;
            size++;
        } else {
            Element current = head;
            while (current.getNext() != null) {
                current = current.getNext();
            }
            current.setNext(el);
            size++;
        }
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public void remove(int index) {
        if (index < 0 || index + 1 > size) {
            System.out.printf("Введите целое число от 0 до %d", size - 1);
        } else if (index == 0) {
            if (size == 1) {
                head.setNext(null);
                size--;
            } else {
                head = head.getNext();
                size--;
            }
        } else {
            Element previous = head;
            Element current = head.getNext();
            for (int i = 1; previous.getNext() != null; i++) {
                if (i == index) {
                    previous.setNext(current.getNext());
                    size--;
                    break;
                } else {
                    previous = current;
                    current = current.getNext();
                }
            }
        }
    }

    @Override
    public void set(int index, Object value) {// понимаю, что тут нет проверки на границы индекса
        Element current = head;
        for (int i = 0; i < size; i++) {
            if (i == index) {
                current.setValue(value);
                break;
            } else {
                current = current.getNext();
            }
        }
    }

    @Override
    public Object get(int index) {
        Element current = head;
        Object answer = null; // понимаю, что тут нет проверки на границы индекса
        for (int i = 0; i < size; i++) {
            if (i == index) {
                answer = current.getValue();
                break;
            } else {
                current = current.getNext();
            }
        }
        return answer;
    }

    @Override
    public String toString() {
        Element current = head;
        String result = "";
        for (int i = 0; i < size; i++) {
            result += current.getValue().toString() + ", ";
            current = current.getNext();
        }
        return result;
    }
}
