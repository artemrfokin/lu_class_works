package Lesson6.Structure;

import java.util.Arrays;

import static javafx.scene.input.KeyCode.T;

// список на основе массива
public class DynamicArray implements Structure {

    private Object[] array;
    private int size; // количество элементов в стурктуре

    public DynamicArray(int initialCapacity) {
        if (initialCapacity <= 0) {
            initialCapacity = 10;
        }
        this.array = new Object[initialCapacity];
    }

    @Override
    public void add(Object value) {
        if (array.length == size) {
            // нужна просто новая ссылка память никуда не денется
            Object[] oldArray = array;
            array = new Object[(int) (size * 1.5d)];
            System.arraycopy(oldArray, 0, array, 0, size); // и с динамическими сработает.
        }
        array[size++] = value;
    }


    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public int size() {
        return size;
    }


    @Override
    public void remove(int index) {
        System.arraycopy(array, index + 1, array, index, (array.length - index - 1));
        size--;
    }

    @Override
    public void set(int index, Object value) {
        array[index] = value;
    }


    @Override
    public Object get(int index) {
        return array[index];
    }


    @Override
    public String toString() {
        return "DynamicArray " + Arrays.toString(array);
    }
}
