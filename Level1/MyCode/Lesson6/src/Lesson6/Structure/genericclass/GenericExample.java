package Lesson6.Structure.genericclass;

import Lesson6.Structure.DynamicArray;

public class GenericExample {
    public static void main(String[] args) {
        GenericClass<String> stringGenericExampleClass = new GenericClass<>();
        stringGenericExampleClass.getValue();

        GenericClass<Integer> integerGenericExampleClass = new GenericClass<>();
        integerGenericExampleClass.setValue(88);

        GenericClass<Object> objectGenericExampleClass = new GenericClass<>();
        objectGenericExampleClass.setValue(new DynamicArray(7));

        GenericClass rawGenericExampleClass = new GenericClass<>();
        stringGenericExampleClass.setValue("OLOLO");
    }
}
