package Lesson6.Structure.genericclass;

public class GenericClass<TYPE> {
    TYPE value;

    public void setValue(TYPE value){
        this.value = value;
    }

    public TYPE getValue(){
        return value;
    }
}
