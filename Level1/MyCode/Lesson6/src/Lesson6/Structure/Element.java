package Lesson6.Structure;

public class Element {
    private Object value;
    private Element next;

    public Element(Object value) {
        this.value = value;
    }

    public Object getValue() {
        return value;
    }

    public void setNext(Element element) {
        this.next = element;
    }

    public Element getNext() {
        return next;
    }

    public void setValue(Object value) {
        this.value = value;
    }
}
