package Lesson6.Structure;

//int
public interface Structure<T> {
    void add(T value);
    T get(int index);
    void remove(int index);
    void set(int index, T value);

    boolean isEmpty();
    int size();
}
