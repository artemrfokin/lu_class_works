package Lesson6.Structure;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class App {
    public static void main(String[] args) {
        Structure<Object> array = new List();
//        Structure<Object> array = new DynamicArray(6);
        // Filling
        array.add("3");
        array.add(7);
        array.add("34");
        array.add("15");
        array.add(new GregorianCalendar(2019, Calendar.MARCH, 25).getTime());
        array.add(78);
        System.out.println("Initial list: " + array);

        // Remove using
        array.remove(1);
        System.out.println("List without one element: " + array);

        // Get using
        System.out.println("One element: " + array.get(3));

        // Set using
        array.set(5, "100");
        System.out.println("List with one rewrited element: " + array);
    }
}
