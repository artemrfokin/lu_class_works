package lesson10;

import java.io.File;
import java.io.IOException;

public class FileExample {
    public static void main(String[] args) throws IOException {
        File file = new File("text.txt"); // объект создался, но не в файловой системе
        File another = new File("text2.txt");

//        System.out.println(another.exists()); // проверка на нулл вообще ничего не даст.
//        another.createNewFile(); // ioException он checked
        boolean result = another.createNewFile(); // false - already exists file
//        System.out.println(result);
//        System.out.println("Abs path " + another.getAbsolutePath());

        File dir = new File("src/");
//        System.out.println(dir.isDirectory()); // isFile

        File anotherDir = new File("test/lesson1/test/create/dir/test");
        boolean isCreated = anotherDir.mkdirs();
//        System.out.println(isCreated);

//        File newFile = new File("test/lesson1/test/create/dir/test/newFile.txt");
//        newFile.createNewFile();
        File newFile = new File("../newFile2.txt");
        newFile.createNewFile();

        newFile.renameTo(new File("newFile2.txt")); // для перемещения
        // директорий не надо создавать новую директорию
    }
}
