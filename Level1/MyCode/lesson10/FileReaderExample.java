package lesson10;

import java.io.*;

public class FileReaderExample {
    public static void main(String[] args) { // IOException - радитель fileNpt Found Exception
        /*
         * InputStream - выдает байт
         * Reader - выдает char
         *
         * OutputStream
         * Writer
         * */
        File file = new File("src/lesson10/text.txt");
//        System.out.println(file.exists());
        BufferedReader bufferedReader = null; // чтобы сразу строки
        try {
            bufferedReader = new BufferedReader(new FileReader(file));
            // читать а не char. Оборачивать можно сколько угодно
            // cntrl K - быстрая выдача.
            String line;
            while ((line = bufferedReader.readLine()) != null) {
//                System.out.println(line);
            }
        } catch (IOException exc) {
            exc.printStackTrace();
        } finally {
            if (bufferedReader != null) {
                try {
                    bufferedReader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        BufferedReader scanner = new BufferedReader(new InputStreamReader(System.in)); // чтение
        scanner.readLine()
        // из консоли через reader
        // try with resourses
        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            String line;
            while ((line = reader.readLine()) != null) {
                System.out.println(line);
            }
        } catch (IOException exc) {
            exc.printStackTrace();
        }
        // при запуске компилятора в консоли надо добавить -classpath.
        // Он будет указывать на место где лежат все импортированные классы
    }
}
