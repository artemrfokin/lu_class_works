package lesson10;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class FileWriterExample {
    public static void main(String[] args) throws IOException {
        File file = new File("src/lesson10/text3.txt");
        file.createNewFile();

        try (BufferedWriter writer = new BufferedWriter(new FileWriter(file, true))){
            for (int i = 0; i < 10; i++){
                writer.write("We love Java!\n");
//                writer.flush(); // сброс буфера.
                // batch - собираем 20 пакетов один раз записываем на диск.
                // сериализация - перевод между форматами
                // маршализация - демаршализация (xml - > )
                // xml - accel
                // JAXB library = java architectire for XML
                // JSON - легче XML: Jackson(Google сделал), Gson, Jpath
                /*
                * база данных. Реляционные PostGree (). SQL.
                * Сервер. TomCat. UI. Nginx.
                * Кеши процессов и согласованность, утечки памяти.
                *
                * "Эккель Философия Java
                * Джошио Блох (Как писать на Java) Affective java.
                * */
            }
            writer.write("qwiodjqw", 2, 3);
        } catch (IOException exc){
            exc.printStackTrace();
        }
    }
}
