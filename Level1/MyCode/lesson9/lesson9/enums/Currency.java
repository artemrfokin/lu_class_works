package lesson9.enums;

public enum Currency {
    /*
    * Три объекта класса Currency
    * */
    EUR("Евро"),
    DOLLAR("Доллар"),
    RUB("Рубль");

    private String russianName;

    Currency(String russianName){
        this.russianName = russianName;
    }

    public String getRussianName() {
        return russianName;
    }

}
