package lesson9.enums;

import java.util.Arrays;
/*
* 1 наследовать от enum нет. Он унаследован от Enum класс
* 2. наследвать enum от другого класса нет тоже самое
* 3. наследовать класс от енум нет. потому что енум - это файнал класс
* 4. енум реализовывать интерфейс да
* 5. наследовать от абстрактного
* 6.
*
* */
public class NumExample {
    public static void main(String[] args) {
        Currency rub = Currency.RUB;
        /*Можно сравнивать через == */
//        System.out.println(rub.name());
//        System.out.println(rub.ordinal());

        Currency fromString = Currency.valueOf("DOLLAR"); // создаем enum из строки
//        System.out.println(fromString == Currency.DOLLAR);

        Currency[] values = Currency.values(); // достаем занчения
        System.out.println(Arrays.toString(values));
//        System.out.println(Currency.RUB.getRussianName());
    }
}
