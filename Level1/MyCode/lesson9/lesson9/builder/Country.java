package lesson9.builder;

import lesson9.enums.Currency;

import java.time.temporal.ChronoUnit;

public class Country {
    private String name;
    private String capital;
    private int peopleCount;
    private double vvp;
    private double square;
    private Currency currency;

    private Country(){}

    public static ElegantCountryBuilder builder(){
        return new Country().new ElegantCountryBuilder();
    }

    @Override
    public String toString() {
        return "Country{" +
                "name='" + name + '\'' +
                ", capital='" + capital + '\'' +
                ", peopleCount=" + peopleCount +
                ", vvp=" + vvp +
                ", square=" + square +
                ", currency=" + currency +
                '}';
    }

    public static class CountryBuilder{
        /*
        * Минус - придется копировать все поля
        * */
        private String name;
        private String capital;
        private int peopleCount;
        private double vvp;
        private double square;
        private Currency currency;

        public CountryBuilder withName(String name){
            this.name = name;
            return this;
        }

        public CountryBuilder withCapital(String capital){
            this.capital = capital;
            return this;
        }

        public CountryBuilder withPeopleCount(int peopleCount){
            this.peopleCount = peopleCount;
            return this;
        }

        public CountryBuilder withVvp(double vvp){
            this.vvp = vvp;
            return this;
        }

        public CountryBuilder withSquare(double square){
            this.square = square;
            return this;
        }

        public CountryBuilder withCurrency(Currency currency){
            this.currency = currency;
            return this;
        }

        private boolean isValid(){
            return name != null &&
                    capital != null &&
                    vvp > 0 &&
                    peopleCount >= 0 &&
                    square > 0 &&
                    currency != null;
        }

        public Country build(){
            if (!isValid()){
                throw new IllegalArgumentException("Fields are not valid");
            }
            Country country = new Country();
            country.name = name;
            country.capital = capital;
            country.peopleCount = peopleCount;
            country.square = square;
            country.vvp = vvp;
            country.currency = currency;

            return country;
        }
    }

    public class ElegantCountryBuilder{
        /*
         * Минус - придется копировать все поля
         * */

        public ElegantCountryBuilder withName(String name){
            Country.this.name = name;
            return this;
        }

        public ElegantCountryBuilder withCapital(String capital){
            Country.this.capital = capital;
            return this;
        }

        public ElegantCountryBuilder withPeopleCount(int peopleCount){
            Country.this.peopleCount = peopleCount;
            return this;
        }

        public ElegantCountryBuilder withVvp(double vvp){
            Country.this.vvp = vvp;
            return this;
        }

        public ElegantCountryBuilder withSquare(double square){
            Country.this.square = square;
            return this;
        }

        public ElegantCountryBuilder withCurrency(Currency currency){
            Country.this.currency = currency;
            return this;
        }

        private boolean isValid(){
            return name != null &&
                    capital != null &&
                    vvp > 0 &&
                    peopleCount >= 0 &&
                    square > 0 &&
                    currency != null;
        }

        public Country build(){
            if (!isValid()){
                throw new IllegalArgumentException("Fields are not valid");
            }
            return Country.this;
        }
    }
}
