package lesson9.builder;

import lesson9.enums.Currency;

public class App {
    public static void main(String[] args) {
        Country country = new Country.CountryBuilder()
                .withName("Russia")
                .withCapital("Moscov")
                .withPeopleCount(143_500_000)
                .withVvp(545)
                .withSquare(9_851_659)
                .withCurrency(Currency.RUB)
                .build();

        System.out.println(country);

        Country usa = Country.builder()
                .withName("USA")
                .withCapital("Washington")
                .withPeopleCount(300_000_000)
                .withVvp(455561)
                .withSquare(2_787_878)
                .withCurrency(Currency.DOLLAR)
                .build();

        System.out.println(usa);
    }
}
