package lesson9;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class StreamAPI {
    public static void main(String[] args) {
        Collection<String> countries = new ArrayList<>();
        countries.add("Russia");
        countries.add("USA");
        countries.add("Canada");
        countries.add("England");
        countries.add("China");
        countries.add("Spain");

//        countries.forEach((country) -> {
//            System.out.println(country);
//            ;});
        int n = 0; // нельзя менять в лябде.
//        countries.forEach(country -> System.out.println(country)); //

//        countries.forEach(System.out::println); //т.к. принимается одно поле и передается абслолютно не изменяя

        // страна длинной больше 5
        Collection<String> filtered = countries.stream()
                .filter(country -> country.length() > 5)
                .collect(Collectors.toList());
        System.out.println(filtered);

        List<Integer> legths = countries.stream()
//                .map(country -> country.length())
                .map(String::length) // потому что ничего не меняли.
                .collect(Collectors.toList());
//        System.out.println();

        int[] values = new int[] {3, 5, 6, 8 ,9, 12}; // no stream
        int[] squares = Arrays.stream(values)
                .map(value -> value * value)
                .toArray();
        System.out.println(Arrays.toString(squares));

        Map<Integer, String> map = new HashMap<>();
        map.forEach((key, value) -> {
            System.out.println(key);
            System.out.println(value);
        });

        map.entrySet().stream()
                .filter(entry -> entry.getKey() > 5)
//                .collect(Collectors.toMap(entry -> entry.getKey(), entry -> entry.getValue()));
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

        Map<Integer, List<String>> group = countries.stream()
                .collect(Collectors.groupingBy(String::length));
        group.entrySet().forEach(entry -> System.out.println(entry.getKey() + " " + entry.getValue().toString()));


    }
}
