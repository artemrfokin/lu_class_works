package lesson8.myexceptions;

public class StackExample {
    public static void main(String[] args) {
        Stack stack = new Stack();
        stack.push();
        stack.push(); // нет никакого предупреждения
        try {
            stack.pop();
            stack.pop();
            stack.pop();
        } catch (EmptyStackException exc){
            exc.printStackTrace();
            //System.exit(0)
        } finally {
            System.out.println("Count " + stack.getCount());
        }
    }
}
