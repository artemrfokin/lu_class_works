package lesson8.myexceptions;

public class Stack {
    private int count;

    public void push() {
        count++;
        if (count == 10) {
            throw new StackOverFlowException();
        }
    }

    public int pop() throws EmptyStackException { // тут нет смысла обрабатывать поэтому отдаем выше
        if (count == 0) {
            throw new EmptyStackException();
        }
        return --count;
    }

    public int getCount() {
        return count;
    }
}
