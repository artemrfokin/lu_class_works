package lesson8.parse;

import java.util.Date;

public class App {
    public static void main(String[] args) {
        Date now = new Date();
//        System.out.println(now);

        DateParser parser = new DateParser();

        Date parcedDate = parser.parse("32.03u.2018");
        System.out.println(parcedDate);
    }
}
