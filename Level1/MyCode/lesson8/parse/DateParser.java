package lesson8.parse;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

// dd.mm.YYYY
public class DateParser {
    public Date parse(String value) //throws ParseException // просто на уровень выше.
            // Но! Если в исходном классе у метода нет throws, то и и в наследнике
            // не всегда надо обрабатывать исключения иногда полезно прокинуть дальшечтобы подобрать еще информацию
    // и выдать сообщение об ошибке с дополнительными данными.
    {
        /*
         * менять как угодно можно
         * d- день
         * M - месяц
         * yyyy - год
         * yy - год
         * h - 12 (am, pm)
         * H - 24
         * m - minutes
         * s - seconds
         * S - milliseconds
         * */
        SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
        //try, catch, finally.
        try {
            return formatter.parse(value);

        } catch (ParseException exc){ // Дети тоже подойдут Throwable
            System.out.println("Invalid");
            return null;
        }
    }
}

/*
 * дата - число в миллисекндах с 01011970.
 *
 * */

