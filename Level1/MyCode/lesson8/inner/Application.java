package lesson8.inner;

import java.util.Comparator;

public class Application {
    public static void main(String[] args) {
        OuterClass outerClass = new OuterClass();
        OuterClass.InnerClass inner = outerClass.new InnerClass();
//      OuterClass.InnerClass inner  new OuterClass().new InnerClass();
        inner.getAndIncrement();
        int increment = inner.getAndIncrement();
        System.out.println(increment);

        OuterClass.Nested nested = new OuterClass.Nested(); // не надо создавать объект
        nested.print();

        // Вложенный класс - может иметь множество экземпляров;
        // Вложенный статичекий
        // Локальный класс - временное хранение данных. И их обработка.
        // Анонимный класс - обычно создается только от абстракных классов и интерфейсов
        // Comparator<String>;
    }
}
