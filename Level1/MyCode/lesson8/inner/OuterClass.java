package lesson8.inner;

public class OuterClass {
    private int field;

    // внутренний класс
    public class InnerClass{
        public int getAndIncrement(){
            return field++;
        }
    }

    private static void printField(){
        System.out.println("Private field");
    }
    // вложенный класс. Можем использовать только статические поля.
    public static class Nested{
        public void print(){
            printField();
        }
    }
}
